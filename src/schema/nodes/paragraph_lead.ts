/*!
 * © 2019 Atypon Systems LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { NodeSpec } from 'prosemirror-model'

export const paragraphLead: NodeSpec = {
  content: 'paragraph_lead_label paragraph_lead_heading paragraph_lead_text',
  group: 'block element',
  selectable: false,
  parseDOM: [{ tag: 'p', context: 'section/' }],
  toDOM: () => ['p', { class: 'paragraph-lead' }, 0],
}

export const paragraphLeadLabel: NodeSpec = {
  content: 'text*',
  selectable: false,
  marks: '',
  inline: true,
  defining: true,
  parseDOM: [{ tag: 'span.paragraph-lead-label' }],
  toDOM: () => ['span', { class: 'paragraph-lead-label' }, 0],
}

export const paragraphLeadHeading: NodeSpec = {
  content: 'text*',
  selectable: false,
  inline: true,
  defining: true,
  marks: '',
  parseDOM: [
    { tag: 'u', context: 'section/paragraph_lead/' },
    { tag: 'span.paragraph-lead-heading' },
  ],
  toDOM: () => ['span', { class: 'paragraph-lead-heading' }, 0],
}

export const paragraphLeadText: NodeSpec = {
  content: 'inline*',
  selectable: false,
  inline: true,
  parseDOM: [{ tag: 'span.paragraph-lead-text' }],
  toDOM: () => ['span', { class: 'paragraph-lead-text' }, 0],
}
