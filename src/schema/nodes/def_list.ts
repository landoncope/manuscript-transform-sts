/*!
 * © 2019 Atypon Systems LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { NodeSpec } from 'prosemirror-model'

export const defList: NodeSpec = {
  content: 'def_item+',
  group: 'block list element',
  selectable: false,
  attrs: {
    id: { default: '' },
  },
  parseDOM: [{ tag: 'dl' }, { tag: 'table.def-list' }],
  toDOM: () => {
    return ['table', { class: 'def-list', draggable: 'false' }, 0]
  },
}

export const defItem: NodeSpec = {
  content: 'def_term def_description',
  group: 'block list element',
  selectable: false,
  parseDOM: [{ skip: true }],
  toDOM: () => ['tr', { class: 'def-item' }, 0],
}

export const defTerm: NodeSpec = {
  content: 'text*',
  selectable: false,
  parseDOM: [{ tag: 'dt' }, { tag: 'td.def-term' }],
  toDOM: () => ['td', { class: 'def-term' }, 0],
}

export const defDescription: NodeSpec = {
  content: 'text*',
  selectable: false,
  parseDOM: [{ tag: 'dd' }, { tag: 'td.def-description' }],
  toDOM: () => ['td', { class: 'def-description' }, 0],
}
